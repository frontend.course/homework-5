const themeKey = "theme";
const light = "light";
const dark = "dark"

let theme = localStorage.getItem(themeKey);

console.log(theme)
if (theme === null) {
    theme = light
}

document.documentElement.classList.add(theme);
document.addEventListener("DOMContentLoaded", function (e) {
    let themeButton = document.querySelector('#change-theme');

    themeButton.addEventListener("click", function (e) {
        const current = theme
        theme = theme === light ? dark : light;
        document.documentElement.classList.replace(current, theme)
        localStorage.setItem(themeKey, theme);
    });
})
